<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Facturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('facturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('factura');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('tipo_pago_id');
            $table->date('fecha');
            $table->foreign('tipo_pago_id')->references('id')->on('tipo_pagos'); 
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
