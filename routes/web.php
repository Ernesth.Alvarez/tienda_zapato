<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::get('/','inicioc@vistaP')->name('ini');
 Route::get('/inicio', 'inicioc@index')->name('inicio');
 Route::get('/usuarios/eliminar/{id}','inicioc@destroy')
 ->where('id', '[0-9]+')//eso espesifica que el parametro 'id' solo puede recivir numeros, sino  no se espesifica el 'id', tambien puede aceptar cadenas de texto
 ->name('eliminar');
 Route::post('/usuarios/nuevo','inicioc@store')->name('nuevoU');
 Route::post('/usuarios/{id}/editar','inicioc@store')
 ->where('id', '[0-9]+')
 ->name('editarU');
 Route::get('/detalleproducto/{id}','inicioc@detalleP')->name('producto');
