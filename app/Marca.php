<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use APP\Producto;

class Marca extends Model
{
    	public function categoria(){
		return $this->hasMany('App\Producto');
	}
}
