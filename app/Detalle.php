<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\Producto;

class Detalle extends Model
{
	public $timestamps = false;
	protected $table='detalles';
	protected $fillable = ['id','cadigo','precio','talla','color'];

    public function productos(){
		return $this->hasMany(Producto::class);
	}
}
