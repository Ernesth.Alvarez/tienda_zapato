<?php

namespace App;
Use Illuminate\Database\Eloquent\Model;
Use App\Detalle;
use APP\Categoria;
use App\Marca;

class Producto extends Model
{
	public $timestamps = false;
	protected $table='productos';
	protected $primaryKey='id';
	protected $fillable = ['id','producto','id_categoria','detalle_id','imegen'];


	public function producto(){
		return $this->hasMany(Producto::class,'detalle_id');
	}
	public function categoria(){
		return $this->belongsTo('App\Categoria');
	}
	public function marcas(){
		return $this->belongsTo('App\Marca');
	}
}
