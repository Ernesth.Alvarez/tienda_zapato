<?php

namespace App\Http\Controllers;
// namespace App\model;


use Illuminate\Http\Request;
use App\Cliente;
use App\Producto;
use App\Detalle;
use App\Categoria;
use Illuminate\Support\Facades\DB;


class inicioc extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //  $v= DB::table('clientes')->get();
        // return view('iniciov',['usuario'=>$v]);
        $v=Cliente::all();
        echo view('layouts/header');
        return view('iniciov',['usuario'=>$v]);
        echo view('layouts/footer');
          // return view('iniciov',$v);//formas de enviar parametros
          // return view('iniciov',compact('v'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=request()->all(); 
        // $usuario=DB::insert('insert into cliente (name,apellido,direccion,email,password) values (?,?,?,?,?)',
        //  [
        // $data['nombre'],
        // $data['apellido'],
        // $data['direccion'],
        // $data['email'],
        // bcrypt($data['clave'])]);
        //-------------------------------------------------------------------------
        // Cliente::create([
        //     'name'      => $data['nombre'],
        //     'apellido'  => $data['apellido'],
        //     'direccion' => $data['direccion'],
        //     'email'     => $data['email'],
        //     'password'  => bcrypt($data['clave'])]
        // );//para utilizar este metedo de crear resgistro es necesario espesificar en el modelo los campos que se van a insertar ejm:protected $fillable = ['name', 'email', 'password',];
        $user = new Cliente;

        $user->name=$data['nombre'];
        $user->apellido=$data['apellido'];
        $user->direccion=$data['direccion'];
        $user->email=$data['email'];
        $user->password=bcrypt($data['clave']);

        $user->save();

        

        return redirect()->Route('inicio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $id)
    {
        $id->find();
       // DB::table('cliente')->where('id', '?')->value($id);
        return redirect()->Route('');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //    $data=request()->all();
        // $usuario=DB::update('update cliente (name,apellido,direccion,email,password) values (?,?,?,?,?)',
        //  [
        // $data['nombre'],
        // $data['apellido'],
        // $data['direccion'],
        // $data['email'],
        // bcrypt($data['nombre'])]);

        // return redirect()->Route('inicio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(/*Cliente*/ $id)
    {
        //$id->delete();//para usar esta forma de eliminar se intancia el modelo en la funcion eliminar y se indeca que id se borrar.
        //-------------------------------------------------------------------------
        //Cliente::find($id)->delete();//esta forma de eliminar se intancia el modelo se indica el metodo buscar y su id, luego se llama al metodo de eliminacion en la misma lina.
        //-------------------------------------------------------------------------
       //$user=Cliente::find($id)->delete();
        //-------------------------------------------------------------------------
       //$user=Cliente::find($id);// guardamos el resultado de la busqueda en una variable en la proxima linea
       //$user->delete();// indicamos que el resultado de la busca lo queremos eliminar.
        //-------------------------------------------------------------------------
        DB::table('cliente')->where('id', '=', $id)->delete();//accedemos a la base y su tabla indicamos con un where la busqueda y luego llamamos al metoso de eliminarsion.

        return redirect()->Route('inicio');// indicamos la redireccion al finalizar el proceso.
    }
    public function vistaP(){
    // $productos= Producto::all();
        // $productos = DB::table('productos')
        // ->join('tallas', 'tallas.id', '=', 'productos.id')
        // ->join('categoria', 'categoria.id', '=', 'productos.id')
        // ->select('productos.*','productos.producto','productos.id', 'tallas.talla', 'tallas.codigo','tallas.color','tallas.precio','imagen')
        // ->get()
        // ->All();

     echo view('partials/header');
     $productos = Producto::with('detalles')->get();
     dd($productos);
     return view('index',[
        // 'productos'=>$productos,
        'productos'=>$productos]);

 }
 public function detalleP(Producto $id){
  $tallas = Producto::with('tallas')->get();

  $data=$id->find($id);  
  echo view('partials/header');
  return view('DetalleProductoV',[
    'data'=>$data,
    'tallas'=>$tallas]);

}
}
