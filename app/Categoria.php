<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Producto;
use App\Marca;

class Categoria extends Model
{
    public function producto(){
    	return $this->hasMany('App\Producto');
    }
     public function marcas(){
    	return $this->hasMany('App\Marca');
    }
}
